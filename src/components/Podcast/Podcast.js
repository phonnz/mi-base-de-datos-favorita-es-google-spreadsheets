import React from 'react';
import cx from 'classnames';

import './Podcast.scss';

export default function Podcast(props) {
  const { podcast, className } = props;

  return (
    <div
      style={{
        backgroundImage: podcast.image && `url(${podcast.image})`,
      }}
      className={cx('Podcast', className)}
    >
      <p className="Podcast__title m-0">{podcast.title}</p>
    </div>
  );
}
