import React from 'react';
import { Link, graphql } from 'gatsby';

import Layout from '../components/layout';
import Podcast from '../components/Podcast';
import SEO from '../components/seo';

const IndexPage = ({ data }) => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <p>Un directorio de podcasts mexicanos chingones</p>

    <ul className="row list-unstyled">
      {data.allPodcast.edges.map(edge => {
        const podcast = edge.node;
        return (
          <li key={podcast.id} className="col-4 mb-4">
            <Link to={`/podcasts/${podcast.slug}`}>
              <Podcast podcast={podcast} />
            </Link>
          </li>
        );
      })}
    </ul>
  </Layout>
);

export default IndexPage;

export const query = graphql`
  query IndexPageQuery {
    allPodcast {
      edges {
        node {
          id
          slug
          title
          image
        }
      }
    }
  }
`;
